﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using net.spottog.I18n.Interfaces;
using net.spottog.I18n.Models;
using net.spottog.I18n.TextSave;

namespace net.spottog.I18n.MariaDBBackend
{
    /// <summary>
    /// Erzeugt einen auf dem Entity Freakwork Bassierenden TextSave.
    /// Als Backend wird eine In MenoryDatabase verwendet.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class MariaDBSave<T> : EFSave<T> where T : class, ITextWithLanguage
    {
        /// <summary>
        /// Erzeugt einen auf MariaDB bzw. MySQL Bassierenden Text Speicher.
        /// </summary>
        /// <param name="next">Nächste textsave, für rekusieve Aufrufe.</param>
        /// <param name="ConnectionString">Mysql Connection String siehe </param>
        public MariaDBSave(ITextSave next, string ConnectionString): base(new DbContextOptionsBuilder<TextContext<T>>().UseMySql(ConnectionString).Options, next)
        {
        }
        /// <summary>
        /// Erzeugt einen auf MariaDB bzw. MySQL Bassierenden Text Speicher.
        /// </summary>
        /// <param name="next">Nächste textsave, für rekusieve Aufrufe.</param>
        /// <param name="host">Der Hostnamen oder die Ip, mit welcher auf die Datenbank zugegriffen werden kann</param>
        /// <param name="database">Namen der Datenbank</param>
        /// <param name="user">Der Benutzernamen für die anmeldung an der Datenbank</param>
        /// <param name="pwd">Das Passwort für die anmeldung an der Datenbank</param>
        /// <param name="port">cdf Port, unter welchem auf die Datenbank zugegriffen werden kann</param>
        public MariaDBSave(ITextSave next, string host, string database, string user, string pwd, int port = 3306) : this(next, $"server={host};port={port};uid={user};pwd={pwd};database={database}") {
        }

    }
}
