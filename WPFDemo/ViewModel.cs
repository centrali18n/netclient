﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using net.spottog.I18n.Models;

namespace WPFDemo
{
    class ViewModel : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged;
        public const int PintSaveSize = 150;

        public void OnPropertyChanged(string propertyName) {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        public ObservableCollection<Point> Wan1Points { get; } = new ObservableCollection<Point>();
        public ObservableCollection<Point> Wan2Points { get; } = new ObservableCollection<Point>();
        private string _OnlineStatus = "Online";

        public string OnlineStatus{
            get { return _OnlineStatus; }
            private set{
                if (!_OnlineStatus.Equals(value)){
                    _OnlineStatus = value;
                    OnPropertyChanged("OnlineStatus");
                }
            }
        }
        private string _Status = "Alles OK";

        public string Status {
            get { return _Status; }
            private set {
                if (!_Status.Equals(value)) {
                    _Status = value;
                    OnPropertyChanged("Status");
                }
            }
        }
        private string _Wan1 = "Wan1";

        public string Wan1 {
            get { return _Wan1; }
            private set {
                if (!_Wan1.Equals(value)) {
                    _Wan1 = value;
                    OnPropertyChanged("Wan1");
                }
            }
        }
        private string _Wan2 = "Wan2";

        public string Wan2 {
            get { return _Wan2; }
            private set {
                if (!_Wan2.Equals(value)) {
                    _Wan2 = value;
                    OnPropertyChanged("Wan2");
                }
            }
        }

        private double value1Now;
        private double value2Now;
        private Random rnd = new Random();
        private readonly Timer timer;
        public ViewModel()
        {
            timer = new Timer(Simulation, null, 0, 300);
        }

        public void FireLanguageChange()
        {

            OnPropertyChanged("Status");
        }
        private void Simulation(object state) {
            value1Now += rnd.NextDouble() * 5 - 2.5;
            value2Now += rnd.NextDouble() * 5 - 2.5;
            if (value1Now < 0 || value1Now > 50) value1Now = 20;
            if (value2Now < 0 || value2Now > 50) value2Now = 20;
            Point pwan1 = new Point(DateTime.Now.Ticks, value1Now);
            Point pwan2 = new Point(DateTime.Now.Ticks, value2Now);
            Wan1 = $"Wan1: {value1Now} ms";
            Wan2 = $"Wan2: {value2Now} ms";
            //try
            //{
                Application.Current?.Dispatcher?.Invoke(new Action(() =>
                {
                    Wan1Points.Add(pwan1);
                    Wan2Points.Add(pwan2);
                    while (Wan1Points.Count >= PintSaveSize) Wan1Points.RemoveAt(0);
                    while (Wan2Points.Count >= PintSaveSize) Wan2Points.RemoveAt(0);
                }));
            //}
            //catch {} //Exception kommt beim beenden wenn schlecht getimt.

            if (value1Now > 40 && value2Now > 40){
                OnlineStatus = "Offline";
                Status = "Beide Verbindungen Gestört";
            }else if (value1Now > 40 || value2Now > 40)
            {
                OnlineStatus = "Online";
                Status = "Eine Verbindung Gestört";
            }else {
                OnlineStatus = "Online";
                Status = "Alles OK";
            }
        }
        public class Point {
            public Point(double X, double Y) {
                this.X = X;
                this.Y = Y;
            }
            public double X { get; set; }
            public double Y { get; set; }
        }

    }
}
