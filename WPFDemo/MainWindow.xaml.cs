﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using net.spottog.I18n.Models;
using net.spottog.I18n.TextSave;

namespace WPFDemo {
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly Timer sekTimer;
        public MainWindow() {
            I18n.Save = new FakeSave<I18n>(null);
            InitializeComponent();
            foreach (var culture in CultureInfo.GetCultures(CultureTypes.InstalledWin32Cultures))
            {
                Button btn = new Button();
                btn.Content = culture.DisplayName;
                btn.Click += (sender, args) => CultureChange(culture, sender, args);
                Languages.Children.Add(btn);
            }
            sekTimer = new Timer(SekTimerCall, null, 0, 1000);
            //CultureChange(CultureInfo.CurrentUICulture);
        }

        private void SekTimerCall(object state){
            Uhrzeit.Dispatcher.BeginInvoke(new Action(() => Uhrzeit.Content = DateTime.Now));
        }

        private void CultureChange(CultureInfo culture, object sender = null, RoutedEventArgs args = null)
        {
            CultureInfo.CurrentUICulture = culture;
            CultureInfo.CurrentCulture = culture;
            CultureInfo.DefaultThreadCurrentCulture = culture;
            CultureInfo.DefaultThreadCurrentUICulture = culture;
            lang.Content = CultureInfo.CurrentUICulture.DisplayName;
            ((ViewModel)this.DataContext).FireLanguageChange();

        }
    }
}
