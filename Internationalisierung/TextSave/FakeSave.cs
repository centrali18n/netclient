﻿using System;
using System.Globalization;
using net.spottog.I18n.Interfaces;
using net.spottog.I18n.Models;

namespace net.spottog.I18n.TextSave
{
    /// <summary>
    /// Fakesave, welcher den Suchstring und die Landeskennung zurückgibt.
    /// Beispiel:
    /// [de]Hallo
    /// </summary>
    /// <typeparam name="T">der Genaue Typ, mit welchem gearbeitet wird.</typeparam>
    public class FakeSave<T> : ITextSave where T : class, ITextWithLanguage
    {

        /// <summary>
        /// Wenn True wird eine Ausgabe auf der Console gemacht.
        /// Wird beim Erzeugen festgelegt.
        /// </summary>
        public readonly bool WarnMsg;
        /// <summary>
        /// Erzeugt einen Fakesave
        /// </summary>
        /// <param name="next">muss null sein sonst exception.</param>
        /// <param name="WarnMsg">wenn true, wird eine Ausgabe auf der Konsole gemacht.</param>
        public FakeSave(ITextSave next = null, bool WarnMsg = false) : base(next)
        {
            this.WarnMsg = WarnMsg;
            if (next != null)
                throw new ArgumentOutOfRangeException("next must be null at FakeSave");
        }

        /// <inheritdoc />
        public override ITextWithLanguage GetLocal(string search, CultureInfo Language)
        {
            if(WarnMsg) Console.WriteLine($"FakeSave Suche Text: [{Language.TwoLetterISOLanguageName}]{search}");
            return new TextWithLanguage(search, $"[{Language.TwoLetterISOLanguageName}]{search}", Language);
        }
    }
    public class FakeSave : FakeSave<TextWithLanguage>
    {
        public FakeSave(ITextSave next = null, bool WarnMsg = false) : base(next, WarnMsg){}
    }
}
