﻿using System.Globalization;

namespace net.spottog.I18n.Interfaces
{
    /// <summary>
    /// Das Interface welches Alle TextSpeicher implementieren. Die TextSpeicher enthalten die eigentlichen Texte. Wenn ein TextSpeicher einen Text nicht enthält befrägt er seinen nachfolger, dadurch wird eine verkettung aufgebaut. Ich Spreche von einem interface, weil es für die Verwendung besser passt implementiert ist allerdings eine abstrakte klasse, da Java interfaces keinerlei implementierungen enthalten können ich die verkettung allerdings in der Basisklasse implementiert haben wollte.
    /// </summary>
    public abstract class ITextSave
    {
        /// <summary>
        /// Gibt das nächste objekt in der verkettung zurück.
        /// </summary>
        public ITextSave Next { get; }
        /// <summary>
        /// Es Wird ein Text in einer Bestimmten Sprache gesucht. @See GetLocal Wird der text nicht gefunden wird der nächste TextSave in der Verkettung gesucht.
        /// </summary>
        /// <param name="search">der zu suchende Text</param>
        /// <param name="Language">die zu suchende Sprache</param>
        /// <returns>das Text Objekt</returns>
        public ITextWithLanguage Get(string search, CultureInfo Language)
        {
            ITextWithLanguage local = GetLocal(search, Language);
            if (null != local && (Next == null || local.Valid))
                return local;
            if (null == Next)
                return null;
            ITextWithLanguage fund = Next.Get(search, Language);
            Add(fund);
            return fund;
        }
        /// <summary>
        /// Die Standardimplementierung von Add Wenn ein TextSpeicher Texte Hunzufügen kann, wird diese Methode überschrieben.
        /// </summary>
        /// <param name="internationaText">das Textobjekt welches hinzugefügt werden soll.</param>
        public virtual void Add(ITextWithLanguage internationaText)
        {
            Next?.Add(internationaText);
        }
        /// <summary>
        /// Es wird Lokal gesucht.
        /// </summary>
        /// <param name="search">der zu suchende Text</param>
        /// <param name="Language">die zu suchende Sprache</param>
        /// <returns>    das Text Objekt bzw. null wenn nichts gefunden wird.</returns>
        public abstract ITextWithLanguage GetLocal(string search, CultureInfo Language);
        /// <summary>
        /// Frägt ab, ob ein TextSpeicher ein Bestimmtes Element enthält
        /// </summary>
        /// <param name="search">das Such Wort, bzw. Satz.</param>
        /// <param name="Language">die Sprache.</param>
        /// <returns>true, wenn ein wert enthalten ist.</returns>
        public bool Contains(string search, CultureInfo Language) => null != GetLocal(search, Language);
        /// <summary>
        /// Instanziiert einen TextSpeicher.
        /// </summary>
        /// <param name="next">das nächste objekt für die verkettung</param>
        public ITextSave(ITextSave next)
        {
            Next = next;
        }
    }
}
