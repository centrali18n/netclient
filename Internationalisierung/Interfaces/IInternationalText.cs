﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;

namespace net.spottog.I18n.Interfaces {  
    /// <summary>
    /// Ein Verkettetes Textelement.
    /// </summary>
    public abstract class IInternationalText : ITextWithLanguage {
        //public IInternationalText()
        //{
        //    //Save?.Add(InternationaText);
        //}
        /// <summary>
        /// Der TextSave, bzw. der Beginn der Verkettung von Verschiedenen TextSave's welche befragt werden.
        /// </summary>
        public static ITextSave Save;
        /// <summary>
        /// Das Textelement in der Verkettung an der Aktuellen Position
        /// </summary>
        public ITextWithLanguage InternationaText { get; protected set; }
        /// <summary>
        /// Reperesentiert das Nächste Element in der Verkettung.
        /// </summary>
        public IInternationalText Second { get; protected set; }

        public string SearchString => Second == null ? InternationaText.SearchString : InternationaText.SearchString + Second.SearchString;
        public string TwoLetterISOLanguageName { get; set; }
        /// <summary>
        /// Die Sprache des Textes.
        /// </summary>
        [NotMapped]
        public CultureInfo Language { get => CultureInfo.GetCultureInfoByIetfLanguageTag(TwoLetterISOLanguageName); set => TwoLetterISOLanguageName = value.TwoLetterISOLanguageName; }
        public string Text { get; set; }
        public DateTime Expire { get; set; }
        [NotMapped]
        public bool Valid => Expire.Ticks > DateTime.Now.Ticks;

        //public ITextWithLanguage ITextWithLanguage {
        //    get => default(ITextWithLanguage);
        //    set {
        //    }
        //}
        /// <summary>
        /// Gibt den Text in der Ausgewählten Sprache zurück.
        /// </summary>
        /// <param name="language">die Sprache</param>
        /// <returns>der Text</returns>
        public abstract string ToString(CultureInfo language);
        /// <summary>
        /// Gibt den Text in der Standard Sprache zurück.
        /// </summary>
        /// <returns>der Text</returns>
        public override string ToString() {
            return ToString(CultureInfo.CurrentUICulture);
        }
    }
}
