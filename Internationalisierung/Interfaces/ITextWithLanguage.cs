﻿using System;
using System.ComponentModel.DataAnnotations;

namespace net.spottog.I18n.Interfaces
{
    /// <summary>
    /// Das Interface eines Textes mit einer Sprache, und einem Ablaufdatum
    /// </summary>
    public interface ITextWithLanguage {
        [Key]
        /// <summary>
        /// ein Wort ein Satz oder ein Teilsatz.
        /// </summary>
        string SearchString { get; }
        /// <summary>
        /// Die Sprache in welcher der Text gesucht wird oder Vorliegt.
        /// </summary>
        [Key]
        string TwoLetterISOLanguageName { get; set; }
        /// <summary>
        /// Der Text zu einem Suchstring in einer Sprache.
        /// </summary>
        String Text { get; set; }
        /// <summary>
        /// Zeitpunkt, zu welchem das element ungültig wird.
        /// </summary>
        DateTime Expire { get; set; }
        /// <summary>
        /// Gibt an ob ein Text noch gültig ist, andersfalls ist er abgelaufen.
        /// </summary>
        bool Valid { get; } // => Expire.Ticks > DateTime.Now.Ticks;

    }
}
