﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace net.spottog.I18n.Models {
    public class WpfConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is string Searchstring))
                Searchstring = (string)parameter;
            return culture;
            return new I18n(Searchstring).ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is string Searchstring))
                Searchstring = (string)parameter;
            return new I18n(Searchstring).ToString();
        }
    }
}
