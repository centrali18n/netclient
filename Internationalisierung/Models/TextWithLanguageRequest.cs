﻿using System;
using System.Globalization;

namespace net.spottog.I18n.Models {
    /// <summary>
    /// Abfrageklasse, in welcher aus dem I18nService ein Text abgefragt werden kann.
    /// </summary>
    public class TextWithLanguageRequest {
        /// <summary>
        /// Der Api Key, welcher im I18n Service erzeugt werden kann.
        /// </summary>
        public String apiKey;
        /// <summary>
        /// Das zu suchende Text element.
        /// </summary>
        public String searchString;
        /// <summary>
        /// Die Sprache, in welcher Gesucht werden soll.
        /// </summary>
        public CultureInfo language;
    }
}
