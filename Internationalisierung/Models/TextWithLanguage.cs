﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using net.spottog.I18n.Interfaces;

namespace net.spottog.I18n.Models
{
    /// <summary>
    /// Ein Text, welcher eine Sprache und ein Ablaufdatum hat.
    /// </summary>
    public class TextWithLanguage : ITextWithLanguage
    {
        /// <summary>
        /// Sollte nicht Verwendet werden, wird aber fürs EntityFreamworkCore gebraucht.
        /// </summary>
        public TextWithLanguage() { }// EntityFreamworkCore
        /// <summary>
        /// Erzeugt einen Text, welcher eine Sprache und ein Ablaufdatum hat.
        /// </summary>
        /// <param name="searchstring"></param>
        /// <param name="text"></param>
        /// <param name="language"></param>
        public TextWithLanguage(string searchstring, string text, CultureInfo language) : this(searchstring, text,
            language, DateTime.Now + TimeSpan.FromMinutes(1))
        {
        }
        /// <summary>
        /// Erzeugt einen Text, welcher eine Sprache und ein Ablaufdatum hat.
        /// </summary>
        /// <param name="searchstring">der nicht internationalisierte Test.</param>
        /// <param name="text">Der Übersetzte Text.</param>
        /// <param name="language">Die Sprache</param>
        /// <param name="expire">das Ablaufdatum</param>
        public TextWithLanguage(string searchstring, string text, CultureInfo language, DateTime expire)
        {
            SearchString = searchstring;
            Text = text;
            Language = language;
            Expire = expire;
        }

        /// <inheritdoc />
        [Key]
        public string SearchString { get; set; }
        /// <inheritdoc />
        [Key]
        public string TwoLetterISOLanguageName { get; set; }
        /// <summary>
        /// Die Sprache
        /// </summary>
        [NotMapped]
        public CultureInfo Language { get => CultureInfo.GetCultureInfoByIetfLanguageTag(TwoLetterISOLanguageName); set => TwoLetterISOLanguageName = value?.TwoLetterISOLanguageName; }
        /// <inheritdoc />
        public string Text { get; set; }
        /// <inheritdoc />
        public DateTime Expire { get; set; }
        /// <inheritdoc />
        [NotMapped]
        public bool Valid => Expire.Ticks > DateTime.Now.Ticks;

        //public ITextWithLanguage ITextWithLanguage {
        //    get => default(ITextWithLanguage);
        //    set {
        //    }
        //}
    }
}
