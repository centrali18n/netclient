﻿using System;
using System.Collections.Concurrent;
using System.Globalization;
using Microsoft.Extensions.Localization;

namespace net.spottog.I18n.Models {
    /// <summary>
    /// Factory, welche die Localizer für die ASP Anwendung erstellt.
    /// </summary>
    public class I18nLocalizerFactory : IStringLocalizerFactory {
        /// <summary>
        /// interne Speicher, damit jede Cultureinfo einen Localizer behält, und nicht jedesmal neue objekte erzeugt werden.
        /// </summary>
        private readonly ConcurrentDictionary<CultureInfo, IStringLocalizer> Save = new ConcurrentDictionary<CultureInfo, IStringLocalizer>();
        /// <summary>
        /// Factory, welche ein Localizer in der Aktuellen Sprache zurück gibt,
        /// </summary>
        /// <param name="resourceSource">nicht verwendet.</param>
        /// <returns>den Localizer</returns>
        public IStringLocalizer Create(Type resourceSource)
        {
            return Create(String.Empty, String.Empty);
        }
        /// <summary>
        /// Factory, welche ein Localizer in der Aktuellen oder gewünschten Sprache zurück gibt,
        /// </summary>
        /// <param name="baseName">wird nicht verwendet.</param>
        /// <param name="location">die Sprache. wenn null oder leer die Aktuelle UI Sprache.</param>
        /// <returns>den Localizer</returns>
        public IStringLocalizer Create(string baseName, string location)
        {
            CultureInfo ci = String.IsNullOrEmpty(location) ? CultureInfo.CurrentUICulture : new CultureInfo(location);
            return Save.GetOrAdd(ci, info => new I18nLocalizer(ci));
        }
    }

}
