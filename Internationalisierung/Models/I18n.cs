﻿using System;
using System.Globalization;
using System.Text;
using Microsoft.Extensions.Localization;
using net.spottog.I18n.Interfaces;

namespace net.spottog.I18n.Models
{
    /// <summary>
    /// Standard Klasse für einen Text.
    /// </summary>
    public class I18n : IInternationalText
    {
        /// <summary>
        /// Gibt die Sprache zurück, in welcher es angelegt wurde.
        /// </summary>
        public new CultureInfo Language => CultureInfo.GetCultureInfoByIetfLanguageTag(InternationaText.TwoLetterISOLanguageName);
        /// <summary>
        /// Erstellt ein Standard Text Element
        /// </summary>
        /// <param name="text">der Text</param>
        /// <param name="cultureInfo">Die Sprache</param>
        public I18n(String text, CultureInfo cultureInfo) : this(text, null, cultureInfo){}
        /// <summary>
        /// Erstellt ein Standard Text Element
        /// </summary>
        /// <param name="text">der Text</param>
        /// <param name="second">Ein Weiteres Object, mit welchem Verkettet wird</param>
        /// <param name="cultureInfo">Die Sprache</param>
        public I18n(String text, I18n second = null, CultureInfo cultureInfo = null) {
                InternationaText = new TextWithLanguage(text, text, cultureInfo);
            Second = second;
            if(cultureInfo != null)
                Save?.Add(InternationaText);
        }
        /// <summary>
        /// Mit Hilfe der LocalizedString kann die Bibliotek als backend für die ASP Internationalisierung verwendet werden.
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        internal LocalizedString GetLocalizedString(CultureInfo language)=> new LocalizedString(SearchString, ToString(language));

        /// <inheritdoc />
        public override string ToString(CultureInfo language)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(Save != null ? Save.Get(InternationaText.SearchString, language).Text : InternationaText.Text);
            if (null != Second)
                sb.Append(Second.ToString(language));
            return sb.ToString();
        }
    }
}
