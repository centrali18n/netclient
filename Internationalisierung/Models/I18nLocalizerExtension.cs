﻿using System;
using Microsoft.AspNetCore.Mvc.Localization;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;
using net.spottog.I18n.Interfaces;

namespace net.spottog.I18n.Models {
    /// <summary>
    /// Aktiviert das net.spottog.I18n Backend für eine net Core Anwendung.
    /// Aufruf in der Startup.cs
    /// Erweitert: IServiceCollection
    /// siehe Beispiel.
    /// </summary>
    public static class I18nLocalizerExtension {
        /// <summary>
        /// Bei Bedarf, wird diese Methode in der public void ConfigureServices(IServiceCollection services) auzfgerufen.
        /// </summary>
        /// <param name="services">der ASP Service</param>
        /// <param name="initITextSave">das backend, mit welchem gearbeitet werden soll.</param>
        public static void AddI18nLocalizer(this IServiceCollection services, ITextSave initITextSave = null) {
            //Setzt den Textsave fest.
            if (initITextSave != null){
                I18n.Save = initITextSave;
            }
            //Aktiviert das Backend, in der ASP Anwendung.
            services.AddSingleton<IStringLocalizerFactory, I18nLocalizerFactory>();
            services.AddTransient(typeof(IStringLocalizer<>), typeof(StringLocalizer<>));
        }
    }
}
