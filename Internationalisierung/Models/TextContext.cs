﻿using System;
using Microsoft.EntityFrameworkCore;
using net.spottog.I18n.Interfaces;

namespace net.spottog.I18n.Models
{
    /// <summary>
    /// TextContext, um das Entity Freamwork als Backend für i18n.spottog.net zu verwenden.
    /// </summary>
    /// <typeparam name="T">Datentyp, welcher gespeichert wird.</typeparam>
    public class TextContext<T> : DbContext where T : class, ITextWithLanguage
    {
        public TextContext(){ }
        /// <summary>
        /// Erzeugz einen Context.
        /// </summary>
        /// <param name="options"></param>
        public TextContext(DbContextOptions<TextContext<T>> options) : base(options)
        {
            
        }
        /// <summary>
        /// Die Texte.
        /// </summary>
        public DbSet<T> Texte { get; set; }
        /// <summary>
        /// Konfiguration
        /// </summary>
        /// <param name="optionsBuilder"></param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                throw new AccessViolationException("Text Context is not yet configured");
            }
        }
        /// <summary>
        /// Bei Modelerstellung wird der Primary Key in dieser Methode erzeugt.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<T>()
                .HasKey(e => new {e.SearchString, e.TwoLetterISOLanguageName});
        }
    }
}
