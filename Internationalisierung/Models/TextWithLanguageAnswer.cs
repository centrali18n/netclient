﻿namespace net.spottog.I18n.Models {
    /// <summary>
    /// Antwort Object, welches minimale Daten Enthält, um Schnell über ein netzwerk Übertragen zu können.
    /// </summary>
    public class TextWithLanguageAnswer {
        /// <summary>
        /// Zeitpunkt, tu welchem der Text Ungültig wird.s
        /// </summary>
        public long expire;
        /// <summary>
        /// Der Übersetzte Text.
        /// </summary>
        public string text;
        /// <summary>
        /// Der Text unter welchem dieses Objekt gefunden werden kann.
        /// </summary>
        public string searchString;
        /// <summary>
        /// Die Sprache, in welcher der Text ist.
        /// </summary>
        public string language;

    }
}
