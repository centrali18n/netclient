﻿using System;
using System.Globalization;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using net.spottog.I18n.Interfaces;
using net.spottog.I18n.Models;
using Newtonsoft.Json;

namespace net.spottog.I18n.TextSave
{
    public class ServiceClient<T> : ITextSave where T : class, ITextWithLanguage {
        /// <summary>
        /// Die URL des i18n.spottog.net Service.
        /// </summary>
        public readonly String Url;
        private readonly String ApiTokken;
        private readonly HttpClient Client = new HttpClient();
        /// <summary>
        /// Erstellt einen Http Client zur Abfrage des i18n Services.
        /// </summary>
        /// <param name="next">der nächste Text Save zur rekusiven abfrage.</param>
        /// <param name="Url">Die URL des i18n.spottog.net Service</param>
        /// <param name="ApiTokken">Der Api Tokken um den Client zu identifizieren. siehe auch die i18n service Dokumentation</param>
        public ServiceClient(ITextSave next, String Url, String ApiTokken) : base(next)
        {
            this.Url = Url + "/rest/text";
            this.ApiTokken = ApiTokken;
            Client.DefaultRequestHeaders.Accept.Clear();
            Client.DefaultRequestHeaders.Accept.Add(MediaTypeWithQualityHeaderValue.Parse("application/json"));
            Client.DefaultRequestHeaders.Add("User-Agent", "net.spottog.i18n Service Client");


        }


        /// <inheritdoc />
        public override ITextWithLanguage GetLocal(string search, CultureInfo Language)
        {
            string send = JsonConvert.SerializeObject(new TextWithLanguageRequest() { apiKey = ApiTokken, language = Language, searchString = search });
            var content = new StringContent(send);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var data = Client.PostAsync(Url, content).Result;
            if(data.StatusCode != HttpStatusCode.OK && data.StatusCode != HttpStatusCode.NoContent)
                throw new HttpRequestException($"Error: {data.StatusCode}");
            String response = data.Content.ReadAsStringAsync().Result;
            if (String.IsNullOrEmpty(response)) return null;
            var anser = JsonConvert.DeserializeObject<TextWithLanguageAnswer>(response);
            DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            var twl = new TextWithLanguage(anser.searchString, anser.text, new CultureInfo(anser.language.Replace('_', '-')), dt.AddMilliseconds(anser.expire));
            return twl;
        }
    }

    public class ServiceCLient : ServiceClient<TextWithLanguage>
    {
        public ServiceCLient(ITextSave next, string Url, string ApiTokken) : base(next, Url, ApiTokken){}
    }
}
