﻿using System;
using net.spottog.I18n.Interfaces;
using net.spottog.I18n.Models;
using net.spottog.I18n.TextSave;

/// <summary>
/// Beispiel Applikation, welche beschreibt, wie mit dem dem client ein i18n.spottog.net service abgefragt wird.
/// </summary>
namespace i18n_client
{
    class Program
    {
        //public const string SERVER = "i18n";
        public const string SERVER = "127.0.0.1";
        public const string URL = "http://"+ SERVER+":8080/i18n-service";
        public const string TOKKEN = "Ns48yc9GZDB5kvVtkliLanbPtQbCS8CRx4j4jc1aLZVYrYjNDP0hw3VMA2IOBbTJ";
        static void Main(string[] args)
        {
            I18n.Save = new ServiceClient<TextWithLanguage>(new FakeSave<TextWithLanguage>(null, true), URL, TOKKEN);




            Console.WriteLine(new I18n("test").ToString());
            Console.ReadLine();
        }
    }
}
