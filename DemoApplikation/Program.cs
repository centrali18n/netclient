﻿using System;
using System.Globalization;
using net.spottog.I18n.MariaDBBackend;
using net.spottog.I18n.TextSave;
using net.spottog.I18n.Models;
using net.spottog.I18n.PostgreSQL;
/// <summary>
/// Eine Beispiel Applikation, welche zeigt wie mit dem i18n Framwork gearbeitet werden kann.
/// </summary>
namespace DemoApplikation
{
    class Program
    {
        static void Main(string[] args)
        {

            //Es wird eine Verkettung des InMemory Speichers mit dem FakeSpeicher initialisiert.
            //I18n.Save = new InMemorySave<TextWithLanguage>(new FakeSave<TextWithLanguage>(null));
            //I18n.Save = new InMemorySave<TextWithLanguage>(new MariaDBSave<TextWithLanguage>(new FakeSave<TextWithLanguage>(null), "192.168.10.18", "i18n", "oskar", "oskar"));
            //I18n.Save = new InMemorySave<TextWithLanguage>(new PostgreSQLSave<TextWithLanguage>(new FakeSave<TextWithLanguage>(null), "i18n", "i18nClient", "i18nClient", "i18n"));
            I18n.Save = new InMemorySave<TextWithLanguage>(new PostgreSQLSave<TextWithLanguage>(new FakeSave<TextWithLanguage>(null), "127.0.0.1", "i18nClient", "i18nClient", "i18n"));
            //Es wird der Deutsche Text "Hallo, wie geht es dir?" zusammengebaut.
            I18n i18n = new I18n("Hallo", new I18n(", ", new I18n("Wie geht es dir?")));
            //Es wird versucht, den Text auf Deutsch und anschließend auf Englisch auszugeben.
            Console.WriteLine("Ohne Gespeichert:");
            Console.WriteLine(i18n.ToString(CultureInfo.GetCultureInfoByIetfLanguageTag("de")));
            Console.WriteLine(i18n.ToString(CultureInfo.GetCultureInfoByIetfLanguageTag("en")));
    //        //Es werden die Texte ins Englische übersetzt und dadurch in der InMemory Datenbank gespeichert.
    //        I18n.Save.Add(new TextWithLanguage("Hallo", "hello", CultureInfo.GetCultureInfoByIetfLanguageTag("en")));
    //        I18n.Save.Add(new TextWithLanguage(", ", ", ", CultureInfo.GetCultureInfoByIetfLanguageTag("en")));
    //        I18n.Save.Add(new TextWithLanguage("Wie geht es dir?", "how are you?", CultureInfo.GetCultureInfoByIetfLanguageTag("en")));
    //new I18n("Hallo", new I18n(", ", new I18n("Wie geht es dir?", CultureInfo.CurrentCulture), CultureInfo.CurrentCulture), CultureInfo.CurrentCulture);
    //        //Es wird erneut der Text auf Englisch ausgegeben.
    //        Console.WriteLine("Gespeichert:");
    //        Console.WriteLine(i18n.ToString(CultureInfo.GetCultureInfoByIetfLanguageTag("en")));
    //        Console.WriteLine(i18n.ToString(CultureInfo.GetCultureInfoByIetfLanguageTag("de")));
            Console.ReadLine();
        }
    }
}
