﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Microsoft.Extensions.Localization;
using net.spottog.I18n.Interfaces;

namespace net.spottog.I18n.Models {
    /// <summary>
    /// net.spottog.i18n als Backend für Microsoft.Extensions.Localization
    /// </summary>
    /// <typeparam name="T">see: IStringLocalizer<T></typeparam>
    public class I18nLocalizer<T> : IStringLocalizer<T>
    {
        /// <summary>
        /// Die Sprache in welcher der Localizer arbietet.
        /// </summary>
        public readonly CultureInfo Culture;
        /// <summary>
        /// Erzeugt einen Localizer
        /// </summary>
        /// <param name="Culture">Die Sprache in welcher der Localizer arbietet</param>
        /// <param name="TextSave">Der Textsave, welcher als Backend arbietet</param>
        public I18nLocalizer(CultureInfo Culture = null, ITextSave TextSave = null)
        {
            this.Culture = Culture ?? CultureInfo.CurrentUICulture;
            if (null != TextSave) {
                I18n.Save = TextSave;
            }
        }
        /// <summary>
        /// Die Ausgabe aller Strings ist nicht Möglich
        /// Methode muss aufgrund IStringLocalizer implementiert werden.
        /// Ein Aufruf wirft eine Exception.
        /// </summary>
        /// <param name="includeParentCultures">-</param>
        /// <returns>es wird eine Exception geworfen weden.</returns>
        public IEnumerable<LocalizedString> GetAllStrings(bool includeParentCultures) {
            throw new NotImplementedException("there is no list of everything");
        }
        /// <summary>
        /// Erzeugt ein neuen Localizer in einer anderen Sprache.
        /// </summary>
        /// <param name="Culture">die Sprache, in welcher der Localizer arbeiten soll</param>
        /// <returns>ein Localizer Objekt</returns>
        public IStringLocalizer WithCulture(CultureInfo Culture) {
            return new I18nLocalizer<T>(Culture);
        }
        /// <summary>
        /// erzeugt einen Übersetzten String aus dem Backend.
        /// </summary>
        /// <param name="name">der Suchstring</param>
        /// <returns>ein String mit einer Sprache.</returns>
        public LocalizedString this[string name] => new I18n(name).GetLocalizedString(Culture);
        /// <summary>
        /// Formatiert einen String mit Hilfe von String.Format und dem i18n Backend.
        /// </summary>
        /// <param name="name">name unter welchem der Format String im Backend zu finden ist.</param>
        /// <param name="arguments">Argumente, welche an String Format übergeben werden.</param>
        /// <returns>Ein Formatierter String in einer Sprache.</returns>
        public LocalizedString this[string name, params object[] arguments] => new LocalizedString(name, String.Format(new I18n(name).ToString(Culture), arguments));
    }

    /// <summary>
    /// net.spottog.i18n als Backend für Microsoft.Extensions.Localization
    /// </summary>
    /// <typeparam name="T">see: IStringLocalizer<T></typeparam>
    public class I18nLocalizer : IStringLocalizer
    {
        private readonly I18nLocalizer<object> Localizer;
        /// <summary>
        /// Die Sprache in welcher der Localizer arbietet.
        /// </summary>
        public CultureInfo Culture => Localizer.Culture;
        /// <summary>
        /// Erzeugt einen Localizer
        /// </summary>
        /// <param name="Culture">Die Sprache in welcher der Localizer arbietet</param>
        /// <param name="TextSave">Der Textsave, welcher als Backend arbietet</param>
        public I18nLocalizer(CultureInfo Culture = null, ITextSave TextSave = null) {
            Localizer = new I18nLocalizer<object>(Culture, TextSave);
        }
        /// <summary>
        /// Die Ausgabe aller Strings ist nicht Möglich
        /// Methode muss aufgrund IStringLocalizer implementiert werden.
        /// Ein Aufruf wirft eine Exception.
        /// </summary>
        /// <param name="includeParentCultures">-</param>
        /// <returns>es wird eine Exception geworfen weden.</returns>
        public IEnumerable<LocalizedString> GetAllStrings(bool includeParentCultures) =>
            Localizer.GetAllStrings(includeParentCultures);
        /// <summary>
        /// Erzeugt ein neuen Localizer in einer anderen Sprache.
        /// </summary>
        /// <param name="Culture">die Sprache, in welcher der Localizer arbeiten soll</param>
        /// <returns>ein Localizer Objekt</returns>
        public IStringLocalizer WithCulture(CultureInfo Culture) => Localizer.WithCulture(Culture);
        /// <summary>
        /// erzeugt einen Übersetzten String aus dem Backend.
        /// </summary>
        /// <param name="name">der Suchstring</param>
        /// <returns>ein String mit einer Sprache.</returns>
        public LocalizedString this[string name] => Localizer[name];
        /// <summary>
        /// Formatiert einen String mit Hilfe von String.Format und dem i18n Backend.
        /// </summary>
        /// <param name="name">name unter welchem der Format String im Backend zu finden ist.</param>
        /// <param name="arguments">Argumente, welche an String Format übergeben werden.</param>
        /// <returns>Ein Formatierter String in einer Sprache.</returns>
        public LocalizedString this[string name, params object[] arguments] => Localizer[name, arguments];
    }
}
