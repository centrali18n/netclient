﻿using Microsoft.EntityFrameworkCore;
using net.spottog.I18n.Interfaces;
using net.spottog.I18n.Models;

namespace net.spottog.I18n.TextSave
{
    /// <summary>
    /// Erzeugt einen auf dem Entity Freakwork Bassierenden TextSave.
    /// Als Backend wird eine In MenoryDatabase verwendet.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class InMemorySave<T> : EFSave<T> where T : class, ITextWithLanguage
    {
        private static readonly DbContextOptions<TextContext<T>> Options = new DbContextOptionsBuilder<TextContext<T>>()
            .UseInMemoryDatabase(databaseName: "InMemoryTextSave")
            .Options;
        /// <summary>
        /// Erzeugt einen Ram bassierenden Text Speicher.
        /// </summary>
        /// <param name="next">nächste textsave, für rekusieve Aufrufe.</param>
        public InMemorySave(ITextSave next): base(Options, next)
        {
        }
    }

    public class InMemorySave : InMemorySave<TextWithLanguage>
    {
        public InMemorySave(ITextSave next) : base(next) {}
    }
}
