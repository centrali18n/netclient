﻿using System.Globalization;
using Microsoft.EntityFrameworkCore;
using net.spottog.I18n.Interfaces;
using net.spottog.I18n.Models;

namespace net.spottog.I18n.TextSave
{
    /// <summary>
    /// Test Speicher auf grundlage des Entity Freamwork Core.
    /// </summary>
    /// <typeparam name="T">Genauer Typ, welcher Gespeichert wird.</typeparam>
    public class EFSave<T> : ITextSave where T : class, ITextWithLanguage
    {
        /// <summary>
        /// Kontext Optionen, welche die Zugriffe des Entity Freamworks regeln.
        /// </summary>
        public readonly DbContextOptions<TextContext<T>> ContextOptions;
        /// <summary>
        /// Bestimmt, ob automatisch neue Texte dem Textspeicher hinzugefügt werden.
        /// </summary>
        public virtual bool AutoAdd { get; } = true;
        /// <summary>
        /// Erzeugt ein TextSpeicher auf grundlage des Entity Freamworks.
        /// </summary>
        /// <param name="ContextOptions">siehe efcore Doku</param>
        /// <param name="next">der Nächste extspeicher.</param>
        public EFSave(DbContextOptions<TextContext<T>> ContextOptions, ITextSave next) : base(next)
        {
            this.ContextOptions = ContextOptions;
        }

        /// <inheritdoc />
        public override ITextWithLanguage GetLocal(string search, CultureInfo language)
        {
            using (var context = new TextContext<T>(ContextOptions))
            {
                return context.Texte.Find(search, language.TwoLetterISOLanguageName);
            }
        }

        /// <inheritdoc />
        public override void Add(ITextWithLanguage internationaText)
        {
            if (internationaText == null) return; // important as constructor call from efcore
            if (AutoAdd)
            {
                T item = (T) internationaText;
                using (var context = new TextContext<T>(ContextOptions))
                {
                    T EFValue = context.Texte.Find(item.SearchString, item.TwoLetterISOLanguageName);
                    if (EFValue == null) {
                        context.Texte.Add(item);
                    }
                    else
                    {
                        EFValue.Expire = item.Expire;
                        EFValue.Text = item.Text;
                        context.Texte.Update(EFValue);
                    }
                    context.SaveChanges();
                }
            }
            base.Add(internationaText);
        }
    }
    public class EFSave : EFSave<TextWithLanguage>
    {
        public EFSave(DbContextOptions<TextContext<TextWithLanguage>> ContextOptions, ITextSave next) : base(ContextOptions, next){}
    }
}
